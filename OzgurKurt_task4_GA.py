#%%
import random

POPULATION_SIZE = 20
BINARY_STRING_LENGTH = 20
CROSSOVER_PROBABILITY = 0.8
MUTATION_PROBABILITY = 0.05
# cut point for x1 and x2
CROSSOVER_CUT_POINT = ( int )( POPULATION_SIZE / 2 )

def main():

    x_min = 0
    x_max = 6
    
    # linear conversion coef, also epsilon
    lin_coef = ( x_max - x_min ) / ( pow( 2, BINARY_STRING_LENGTH/2 ) - 1 )

    # Create starting population
    currentPopulation = createPopulation()

    ## find fitness values for population
    for gt in currentPopulation:
        gt.set_fitness( calculateFitness( gt, lin_coef, x_min ) )

    counter = 1
    print( str(counter) + "," + str(calculateGenerationMeanFiness( currentPopulation )) )
    
    # until a solution is found
    while True:
    
        counter += 1        
        # Run selection algorithm on population
    
        # Chosen selection type should be uncommented
        parentPopulation = rouletteWheelSelection( currentPopulation )
        #parentPopulation = rankingSelection( currentPopulation )
        #parentPopulation = tournamentSelection( currentPopulation, 2 )

        intermediatePopulation = []

        # Crossover    
        i = 0
        while i < POPULATION_SIZE - 1:

            if random.random() < CROSSOVER_PROBABILITY:
                
                try:
                    # do crossover, calculate new fitness value
                    a, b = crossoverStrings( parentPopulation[ i ].get_string(), 
                                             parentPopulation[ i + 1 ].get_string() )
                    g1 = Genotype( a )
                    g1.set_fitness( calculateFitness( g1, lin_coef, x_min ) )
                    intermediatePopulation.append( g1 )
                    g2 = Genotype( b )
                    g2.set_fitness( calculateFitness( g2, lin_coef, x_min ) )
                    intermediatePopulation.append( g2 )

                except RuntimeError as e:
                    print( "Error" )
                    continue;

            else:
                # add to intermediate population if no crossover is to be applied
                intermediatePopulation.append( parentPopulation[ i ] )
                intermediatePopulation.append( parentPopulation[ i + 1 ] )

            i += 2

        # Mutation
        for i in range( 0, len( intermediatePopulation ) ):

            s, b = mutateString( intermediatePopulation[ i ].get_string() )
            if b:# calculate new fitness value
                intermediatePopulation[ i ].set_string( s )
                intermediatePopulation[ i ].set_fitness(
                        calculateFitness( intermediatePopulation[ i ], lin_coef, x_min )
                    )

        #intermediatePopulation = intermediatePopulation + parentPopulation
        ### sort populations according to their fitness values
        intermediatePopulation.sort( key = lambda x: x.fitness, reverse = True )
        # cut to population size
        #intermediatePopulation = intermediatePopulation[ 0 : POPULATION_SIZE ]

        #print( "sorted: " + intermediatePopulation[0].get_string() +  "  " + str(intermediatePopulation[0].get_fitness()) )
        print( str(counter) + "," + str(calculateGenerationMeanFiness( intermediatePopulation ) ) )

        ## quit if desired value acquired
        if intermediatePopulation[ 0 ].get_fitness() >= ( 1 - lin_coef ):
            print( str( convertVariables( intermediatePopulation[ 0 ].get_string(), 
                                         lin_coef, x_min ) ) )
            print( "after " + str(counter ) + " generations..." )
            break;
        else:
            currentPopulation = intermediatePopulation


def calculateGenerationMeanFiness( population ):
    total = 0
    for a in population:
        total += a.get_fitness()
    return total / len( population )

def rouletteWheelSelection( population ):

    totalFitness = 0
    # calculate the average fitness for the whole population
    for a in population:
        totalFitness += a.get_fitness()

    avg_fitness = totalFitness / len( population )

    total_exp_count = 0
    t = 0
    # calculate each genotype's expected count
    for a in population:
        t = a.get_fitness() / avg_fitness
        a.set_expected_count( t )
        total_exp_count = total_exp_count + t

    # calculate selection probabilities
    for a in population:
        a.set_selection_prob( a.get_expected_count() / total_exp_count )

    # order according to expected counts
    population.sort( key = lambda x: x.selection_prob, reverse = False )

    # set cumulative probabilities
    tt = 0 # temp total
    for a in population:
        tt += a.get_selection_prob()
        a.set_cumulative_prob( a.get_selection_prob() + tt )

    parentPopulation = []
    # create random numbers and get the parents according to their cumulative prob
    for i in range( 0, POPULATION_SIZE ):

        r = random.random()
        ind = 0
        for ind in range( len( population ) ):
            if r <= population[ ind ].get_cumulative_prob():
                break
        parentPopulation.append( population[ ind ] )

    return parentPopulation


def rankingSelection( population ):

    # order by fitness value
    population.sort( key = lambda x: x.fitness, reverse = False )

    rank = 0
    total_rank = 0
    # set ranks for genotypes
    for a in population:
        rank += 1
        a.set_rank( rank )
        total_rank += rank

    # calculate cumulative probabilities
    for a in population:
        a.set_selection_prob( a.get_rank() / total_rank )

    # set cumulative probabilities
    tt = 0 # temp total
    for a in population:
        tt += a.get_selection_prob()
        a.set_cumulative_prob( a.get_selection_prob() + tt )

    parentPopulation = []
    # create random numbers and get the parents according to their cumulative prob
    for i in range( 0, POPULATION_SIZE ):

        r = random.random()
        ind = 0
        for ind in range( len( population ) ):
            if r <= population[ ind ].get_cumulative_prob():
                break
        parentPopulation.append( population[ ind ] )

    return parentPopulation


def tournamentSelection( population, tournament_size ):

    parentPopulation = []    

    # order by fitness value
    population.sort( key = lambda x: x.fitness, reverse = False )
    
    # copy the fittest genotype into mating pool
    parentPopulation.append( population[ 0 ] )
    
    # temp array to hold tournament_size genotypes
    tp = []
    
    # run the tournament selection N-1 times ( elite genotype has already been copied)
    for a in range( 0, POPULATION_SIZE - 1 ):
        for r in range( 0, tournament_size ):
            tp.append( population[ ( int )( random.random() * POPULATION_SIZE ) ] )
        tp.sort( key = lambda x: x.fitness, reverse = True )
        parentPopulation.append( tp[ 0 ] )
        tp = []

    return parentPopulation

def convertVariables( gsl, lin_coef, x_min ):
    
    ## decode the values
    x1str = "".join( gsl[ 0 : CROSSOVER_CUT_POINT ] )
    x2str = "".join( gsl[ CROSSOVER_CUT_POINT : POPULATION_SIZE ] )
    
    ## linear conversion
    x1_con = x_min + int( x1str, 2 ) * lin_coef
    x2_con = x_min + int( x2str, 2 ) * lin_coef    

    #print( str(int( x1str, 2 )) + " = " + str(x1_con) + " | " + str(int( x2str, 2 )) + "  = " + str(x2_con) + "  | " + str(lin_coef ) )

    return x1_con, x2_con


def calculateFitness( gt, lin_coef, x_min ):

    # get genotype string
    gs = gt.get_string()
    gsl = list( gs )

    # Evaluate each string for fitness
    x1_con, x2_con = convertVariables( gsl, lin_coef, x_min )
    ## find function value
    f = testFunction( x1_con, x2_con )
    #print( "function value " + str(f) + " fit: " + str( 1 / ( 1 + f ) ) )

    ## find fitness
    return 1 / ( 1 + f )


def createPopulation():
    
    pList = []
    # a number between 0 - 1048575, formatted in 20 digit binary
    for i in range( 0, POPULATION_SIZE ):
        #print(i)
        pList.append( Genotype( 
                            ( "{0:b}".format( random.randint( 0, 1048576 ) ) )
                            .zfill( BINARY_STRING_LENGTH ) 
                            )
                    );
    return pList

def crossoverStrings( str1, str2 ):

    # assume both strings are of the same length
    sl = len( str1 )
    # crossover point
    cp = int( sl * random.random() )

    str1 = list( str1 )
    str2 = list( str2 )
    ts = str1.copy()    
    str1[ cp : sl ] = str2[ cp : sl ]
    str2[ cp : sl ] = ts[ cp : sl ]

    return "".join( str1 ), "".join( str2 ) 


def mutateString( string ):

    changed = False
    sl = list( string )

    for mp in range( 0, len( sl ) ):

        if random.random() < MUTATION_PROBABILITY:            
            if sl[ mp ] == '0':
                sl[ mp ] = '1'
            else:
                sl[ mp ] = '0'
            changed = True

    return "".join( sl ), changed


def testFunction( x1, x2 ):
    return pow( (pow(x1, 2) + x2 - 11), 2 ) + pow( ( x1 + pow( x2, 2 ) - 7 ), 2 )


class Genotype:

    def __init__( self, string, fitness = 0 ):
        self.string = string
        self.fitness = fitness
        self.expected_count = 0
        self.selection_prob = 0
        self.cumulative_prob = 0
        self.rank = 0

    def get_string( self ):
        return self.string
    
    def set_string( self, string ):
        self.string = string
    
    def get_fitness( self ):
        return self.fitness
    
    def set_fitness( self, fitness ):
        self.fitness = fitness
        
    def get_expected_count( self ):
        return self.expected_count
    
    def set_expected_count( self, expected_count ):
        self.expected_count = expected_count

    def get_selection_prob( self ):
        return self.selection_prob
    
    def set_selection_prob( self, total_expected_counts ):
        self.selection_prob = total_expected_counts
        #self.selection_prob = self.expected_count / total_expected_counts
        
    def get_cumulative_prob( self ):
        return self.cumulative_prob
    
    def set_cumulative_prob( self, cumulative ):
        self.cumulative_prob = cumulative

    def get_rank( self ):
        return self.rank
    
    def set_rank( self, rank ):
        self.rank = rank

#%%