* (Java) 8 Queens: A genetic algorithm to solve the 8 Queens problem (Put 8 queens on a chess board in a way that none of them checks another one)
* (Python) task4_GA: Application of genetic algorithm, with different selection mechanism options
* (Python) task5_Swarm: Application of swarm optimisation
* (Python) task5_SimulatedAnnealing: Application of optimisation using simulated annealing
* (Python) Min_Bisection: Finding the minimum of a function, using bisection method
