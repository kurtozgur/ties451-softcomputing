package fi.kurt.ties451.eightqueens;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Random;


/*
 * Penalty of one queen: the number of queens she can check.
 *
 * Penalty of a configuration: the sum of the penalties of all queens.
 * Note: penalty is to be minimized
 * Fitness of a configuration: inverse penalty to be maximized
 * 
 * Starting Population	: 40
 * Children				: 2
 * Mutation Probability : 80%
 * Parent Selection		: best 2 out of 5
 * Survival Selection	: replace worst
 * */

@SuppressWarnings( "unchecked" )
public class EightQueenSolver {

	public static int fitnessEvaluationCount = 0;
	
	private ArrayList<Genotype> populationList;
	private Genotype finalGenotype;
	private final char[] boardLetters = new char[]{ 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h' };
	private final int MAX_NUMBER_OF_EVALUATIONS = 10000;
	private final int SHUFFLE_ITERATION_COUNT = 200;
	private final int POPULATION_COUNT = 40;
	private final int PARENT_BASE_COUNT = 5;
	
	public static void main( String[] a ){
		new EightQueenSolver();
	}
	
	public EightQueenSolver(){

		this.populationList = new ArrayList<Genotype>( POPULATION_COUNT );
		// genotype
		int[] gen;		

		// initialise starting population with randomised solutions
		for( int i = 0 ; i < POPULATION_COUNT ; i++ ){

			gen = new int[]{ 0, 1, 2, 3, 4, 5, 6, 7 };
			this.shuffler( gen );
			this.populationList.add( new Genotype( gen ) );
			gen = null;

		}//end of for loop

		ArrayList<Genotype> childrenList;
		while( true ){

			/*
			 * Produce children
			 * Mutation + Recombination
			 * */
			childrenList = this.createChildren();
			// add children to population
			this.populationList.addAll( childrenList );

			// sort the resulting population
			Collections.sort( this.populationList, new GenotypeFitnessComparator() );
					
			// if the first element is not fit enough 
			// get the fittest first POPULATION_COUNT elements and continue...
			if( this.populationList.get( 0 ).fitness == 1 ){

				// result is the element
				this.finalGenotype = this.populationList.get( 0 );
				// print final result
				System.out.println( "Final Genotype:" );

				for( int i = 0 ; i < 8 ; i++ ){
					System.out.println( "Q" + this.boardLetters[ i ] + ( this.finalGenotype.gen[ i ] + 1 ) + "," );
				}
				break;				

			}else if( fitnessEvaluationCount >= MAX_NUMBER_OF_EVALUATIONS ){

				System.out.println( "Maximum number of evaluations have been reeached. Best match is:" );
				this.finalGenotype = this.populationList.get( 0 );
				for( int i = 0 ; i < 8 ; i++ ){
					System.out.println( "Q" + this.boardLetters[ i ] + ( this.finalGenotype.gen[ i ] + 1 ) + "," );
				}
				break;

			}else{
				this.populationList = new ArrayList<Genotype>( this.populationList.subList( 0, POPULATION_COUNT ) );
				childrenList = null;
			}

		}//end of while loop

	}//end of constructor

	
	/*
	 * Mutations and recombinations
	 * 
	 * */
	public ArrayList<Genotype> createChildren(){
		
		ArrayList<Genotype> childrenList = new ArrayList<Genotype>();
		ArrayList<Genotype> parentBase = new ArrayList<Genotype>( PARENT_BASE_COUNT );
		
		if( this.populationList != null && this.populationList.size() > 0 ){
			
			Object[] to;
			Random rand = new Random();
			Genotype tg;
			int[] ta;
			int ti1, ti2, tv;

			while( childrenList.size() <= POPULATION_COUNT ){

				tg = null;
				// mutate a random genotype with 80 percent probability (80% of 99 is 79.2, so <80 should do ok)
				if( new Random().nextInt( 100 ) < 80 ){
					
					// find two random indexes to swap
					ti1 = rand.nextInt( 8 );
					do{
						ti2 = rand.nextInt( 8 );
					}while( ti1 == ti2 );
					ta = this.populationList.get( rand.nextInt( POPULATION_COUNT ) ).gen.clone();
					tv = ta[ ti1 ];
					ta[ ti1 ] = ta[ ti2 ];
					ta[ ti2 ] = tv;
					childrenList.add( new Genotype( ta ) );
					ta = null;

				}

				// recombination
				parentBase.clear();
				// find 5 random parent candidates
				for( int j = 0 ; j < PARENT_BASE_COUNT ; j++ ){
					
					do{
						tg = this.populationList.get( rand.nextInt( POPULATION_COUNT ) );	
					}while( parentBase.contains( tg ) );
					parentBase.add( tg );

				}//end of inner loop for random parent bases 
				// random genotypes in parentBase list, we'll select the best 2
				Collections.sort( parentBase, new GenotypeFitnessComparator() );
				to = this.recombine( parentBase.get( 0 ), parentBase.get( 1 ) );
				try{
					childrenList.add( new Genotype( ( int[] )to[ 0 ] ) );
					childrenList.add( new Genotype( ( int[] )to[ 1 ] ) );	
				}catch( Exception e ){
					e.printStackTrace();
				}
				to = null;
				
			}//end of child producing loop
			
			// cut child size to population size
			childrenList = new ArrayList<Genotype>( childrenList.subList( 0, POPULATION_COUNT ) );
						
		}//end of list check

		return childrenList;
		
	}//end of createChildren method
	
	
	/*
	 * Find a recombination from given two parents
	 * */
	public Object[] recombine( Genotype g1, Genotype g2 ){
		
		Object[] ro = new Object[ 2 ];
		
		int[] p1 = g1.gen;
		int[] p2 = g2.gen;
		int[] c1 = new int[ 8 ];
		int[] c2 = new int[ 8 ];
		// find a random slicing point (there's no point for producing a slicing point as the last item in the list)
		int si = new Random().nextInt( 7 );
		
		// copy first part from slicing point to children
		for( int i = 0 ; i <= si ; i++ ){
			c1[ i ] = p1[ i ];
			c2[ i ] = p2[ i ];
		}

		/*
			 create second part by inserting values from other parent: 
			in the order they appear there 
			beginning after crossover point
			 skipping values already in child
		 * */
		int j;

		// loop for second part's new values - c2
		for( int i = si+1 ; i < 8; i++ ){
			
			j = i;
			// if parent has the value from other parent in the first part already, skip it
			while( true ){

				for( int x = 0 ; x < i ; x++ ){
					if( c2[ x ] == p1[ j ] ){
						j++;
						x = -1;
						if( j == 8 ){//start from beginning
							j = 0;
						}
					}
				}

				c2[ i ] = p1[ j ];
				break;
				
			}//end of while loop

		}//end of c1 recombination loop

		// loop for second part's new values - c1
		for( int i = si+1 ; i < 8; i++ ){
			
			j = i;
			// if parent has the value from other parent in the first part already, skip it
			while( true ){

				for( int x = 0 ; x < i ; x++ ){
					if( c1[ x ] == p2[ j ] ){
						j++;
						x = -1;
						if( j == 8 ){//start from beginning
							j = 0;
						}
					}
				}

				c1[ i ] = p2[ j ];
				break;
				
			}//end of while loop

		}//end of c1 recombination loop
		
		ro[ 0 ] = c1;
		ro[ 1 ] = c2;
		return ro;
		
	}//end of recombine method


	/*
	 * Shuffles the given array elements
	 * swap two random elements of the array in SHUFFLE_ITERATION_COUNT iteration counts
	 * */
	private void shuffler( int[] param ){

		Random rand = new Random();
		int a, b, t;
		for( int i = 0 ; i < SHUFFLE_ITERATION_COUNT ; i++ ){
			
			a = rand.nextInt( 8 );
			t = param[ a ];
			while( ( b = rand.nextInt( 8 ) ) == a ){}
			// swap
			param[ a ] = param[ b ];
			param[ b ] = t;

			// reset
			a = 0; 
			b = 0; 
			t = 0;
			
		}// end of for loop		

	}//end of method

	
    /* Inner sorting class ( according to fitness ) for genotype objects */
    class GenotypeFitnessComparator implements Comparator{
    	public final int compare( Object gen1, Object gen2 ){
    		return -1 * Double.compare( ( ( Genotype )gen1 ).fitness, ( ( Genotype )gen2 ).fitness );//desc
    	}    	
    }//end of inner DataLineFilterTypeComparator class
	
}//end of class
