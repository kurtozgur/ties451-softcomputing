package fi.kurt.ties451.eightqueens;

import java.util.Arrays;

public class Genotype extends Object{

	public int[] gen;
	public double fitness;
	
	public Genotype( int[] gen ){
		this.gen = gen;
		this.calculateFitness();
		EightQueenSolver.fitnessEvaluationCount++;	
	}//end of constructor


	/*
	 * g[ i ] != g[ i - n ] & g[ i ]  != g[ i + n ]
	 * g[ i - n ] != g[ i ] - n & g[ i - n ] != g[ i ] + n
	 * g[ i + n ] != g[ i ] - n & g[ i + n ] != g[ i ] + n
	 * */
	public void calculateFitness(){

		int totalPenalty = 0;
		int v = 0;

		// horizontal clash
		for( int i = 0 ; i < 8 ; i++ ){

			v = this.gen[ i ];
			//descending
			for( int n = 1 ; ( i - n ) >= 0 ; n++ ){
				if( v == this.gen[ i - n ] ){					
					totalPenalty++;
				}
			}
			//ascending
			for( int n = 1 ; ( i + n ) < 8 ; n++ ){
				if( v == this.gen[ i + n ] ){			
					totalPenalty++;
				}
			}

		}
		
		// diagonal clash
		for( int i = 0 ; i < 8 ; i++ ){
			
			v = this.gen[ i ];
			//descending
			for( int n = 1 ; ( i - n ) >= 0 ; n++ ){
				if( ( v - n ) == this.gen[ i - n ] || ( v + n ) == this.gen[ i - n ] ){
					totalPenalty++;
				}
			}
			
			//ascending
			for( int n = 1 ; ( i + n ) < 8 ; n++ ){
				if( ( v - n ) == this.gen[ i + n ] || ( v + n ) == this.gen[ i + n ] ){
					totalPenalty++;
				}
			}
			
		}
		
		this.fitness = ( totalPenalty == 0 ? 1 : (double)1 / totalPenalty );

	}//end of calculateFitness method

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(fitness);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + Arrays.hashCode(gen);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Genotype other = (Genotype) obj;
		if (!Arrays.equals(gen, other.gen))
			return false;
		return true;
	}

}//end of method
