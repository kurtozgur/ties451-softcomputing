
"""
    find the minimum point for a function in the interval a-b
    using BISECTION METHOD
"""
#%%
def bisectionOptimisation( a, b, mp = None, 
                           f_a = None, f_b = None, f_m = None ):

    # acceptable tolerance = b - a
    tol = 0.01
    
    # max 100 tries
    for i in range( 100 ):
        
        if mp is None:
            mp = (a + b) / 2 # middle point

        if b - a < tol:
            return mp

        # function for a 
        if f_a is None:
            f_a = testFunction( a )
        # function for b    
        if f_b is None:
            f_b = testFunction( b )
        # function for mid point
        if f_m is None:
            f_m = testFunction( mp )

        # find left-right points
        x_l = (a + mp) / 2
        x_r = (b + mp) / 2

        f_l = testFunction( x_l )#for xl
        f_r = testFunction( x_r )#for xr

        # find minimum of these function values
        mv = min( f_a, f_b, f_m, f_l, f_r )
        
        if mv == f_a or mv == f_l:
            b = mp
            mp = x_l
            f_b = f_m
            f_m = f_l
            f_a = None
        elif mv == f_m:
            a = x_l
            b = x_r
            f_a = f_l
            f_b = f_r
            f_m = None
            mp = None
        elif mv == f_r or mv == f_b:
            a = mp
            mp = x_r
            f_a = f_m
            f_m = f_r
            f_b = None
        
    print( "Max. number  of iterations has been reached" )
    return mp


"""
    For the interval 0 <= x <= 5
    result: 2.998046875
"""
def testFunction( x ):
    # function
    try:
        return pow( x, 2 ) + 54 / x    
    except : 
        return float( "Inf" )

#%%