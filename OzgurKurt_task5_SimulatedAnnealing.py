#%%
import random
import math


class Point:

    def __init__( self, x, y ):
        self.x = x
        self.y = y

    def update( self, point ):
        self.x = point.x
        self.y = point.y

    def get_x( self ):
        return self.x
    
    def set_x( self, x ):
        self.x = x
    
    def get_y( self ):
        return self.y
        
    def set_y( self, y ):
        self.y = y

    def trim_to_limits( self, max, min ):
        if self.x > max.get_x():
            self.x = max.get_x()
        if self.x < min.get_x():
            self.x = min.get_x()
        if self.y > max.get_y():
            self.y = max.get_y()
        if self.y < min.get_y():
            self.y = min.get_y()        

    def printPoint( self ):
        print( "Point x: " + str( self.x ) +  " - Point y: " + str( self.y ))




epsilon = 0.001
n = 100
min_x = 0
max_x = 5


"""
3,2
-3.78,-3.28
-2.8,3.13
3.58,-1.84
"""

def main():

    xi = Point( 2.5, 2.5 )
    std_dev = ( ( max_x - min_x ) / 2 ) / 3
    
    # calculate starting T
    T = ( test_function( Point( min_x, min_x ) ) + test_function( Point( min_x, max_x ) ) + 
         test_function( Point( max_x, min_x ) ) + test_function( Point( max_x, max_x ) ) ) / 4
    t = 0

    while T > 1:
        
        t += 1
        xi2 = Point(0,0)
        
        # random.gauss( mean, std_dev )
        xi2 = Point(
                random.gauss( xi.get_x(), std_dev ),
                random.gauss( xi.get_y(), std_dev )
                )

        delta_E = calculate_distance( xi, xi2 )
        if acceptance_test( xi, xi2, T ):
            xi.update( xi2 )

        if delta_E <= epsilon and T < 10:
            xi.printPoint()
            xi2.printPoint()
            print( "epsilon reached, distance = " + str( calculate_distance( xi, xi2 ) ) )
            break

        if t % n == 0:
            T *= 0.5
    
    print( "Minimum point: " )
    xi.printPoint()
       
    
def calculate_distance( p1, p2 ):
    dx = abs( p1.get_x() - p2.get_x() )
    dy = abs( p1.get_y() - p2.get_y() )
    return math.sqrt( ( pow( dx, 2 ) + pow( dy, 2 ) ) )


def acceptance_test( p1, p2, T ):
    
    f1 = test_function( p1 ) 
    f2 = test_function( p2 ) 
    if f2 < f1:
        return True
    else:
        return random.random() < math.exp( ( f1 - f2 ) / T )

def test_function( p ):
    return pow( ( pow( p.get_x(), 2 ) + p.get_y() - 11 ), 2 ) + pow( ( p.get_x() + pow( p.get_y(), 2 ) - 7 ), 2 ) 



#%%