#%%#%%
import random

class Point:

    def __init__( self, x, y ):
        self.x = x
        self.y = y

    def update( self, point ):
        self.x = point.x
        self.y = point.y

    def get_x( self ):
        return self.x
    
    def set_x( self, x ):
        self.x = x
    
    def get_y( self ):
        return self.y
        
    def set_y( self, y ):
        self.y = y

    def trim_to_limits( self, max, min ):
        if self.x > max.get_x():
            self.x = max.get_x()
        if self.x < min.get_x():
            self.x = min.get_x()
        if self.y > max.get_y():
            self.y = max.get_y()
        if self.y < min.get_y():
            self.y = min.get_y()        

    def printPoint( self ):
        print( "Point x: " + str( self.x ) +  " - Point y: " + str( self.y ))             

class Particle:

    def __init__( self, point, best_fitness ):
        self.current = Point( point.get_x(), point.get_y() )
        # personal best
        self.personal_best = Point( point.get_x(), point.get_y() )
        self.personal_best_fitness = best_fitness
        # current velocity
        self.velocity = Point( 0, 0 )

    def get_current( self ):
        return self.current
    
    def set_current( self, current ):
        self.current = current
        
    def get_personal_best( self ):
        return self.personal_best
    
    def get_personal_best_fitness( self ):
        return self.personal_best_fitness
    
    def set_personal_best( self, best_point, best_fitness ):
        self.personal_best = best_point
        self.personal_best_fitness = best_fitness

    def get_velocity( self ):
        return self.velocity
    
    def set_velocity( self, velocity ):
        self.velocity = velocity


POPULATION_SIZE = 40
c1 = 2
c2 = 2
global_best = Point( 0, 0 )
global_best_fitness = 0
vel_max = Point( 100, 100 )
vel_min = Point( -100, -100 )
epsilon = 0.0025

def main():

    global_best_fitness = 0    
    population = create_population()

    # optimise population
    while True:

        for i in population:

            update_particle_position( i )
            # fitness of this new point
            fi = test_fitness_function( i.get_current() )
            # update particle's and global best positions
            if fi > i.get_personal_best_fitness():
                i.set_personal_best( i.get_current(), fi )
            if fi > global_best_fitness:
                global_best.update( i.get_current() )
                global_best_fitness = fi

        print( global_best_fitness ) 
        if global_best_fitness >= 1 - epsilon:
            break

    # print results        
    print( "Global best point( Minimum for the function ) is: \n\r" + 
           "x: " + str(global_best.get_x()) + ", y: " + str(global_best.get_y()) + "\n\r" +
           "Fitness: " + str(global_best_fitness) )    


def create_population():

    pList = []

    for i in range( 0, POPULATION_SIZE ):

        p = None
        # initialise the position for each particle        
        # a random point between max and min points
        # assuming global velocity minimum is negative, following formula should be ok
        x = ( random.random() * vel_max.get_x() ) + ( random.random() * vel_min.get_x() )
        y = ( random.random() * vel_max.get_y() ) + ( random.random() * vel_min.get_y() )
        point = Point( x, y )
        p = Particle( point, test_fitness_function( point ) )
        pList.append( p )

    return pList


def update_particle_position( particle ):
    
    pb = particle.get_personal_best()
    pc = particle.get_current()
    
    # new velocity
    """
    vn = ( pb.substract( pc ).multiply( c1 * random.random() ) ).
             add( global_best.substract( pc ).multiply( c2 * random.random() ) ).
                add( particle.get_velocity() )
    """
    vn = pointSum( 
        particle.get_velocity(),
        pointMultiply( pointSubstract( pb, pc ), ( c1 * random.random() ) ),
        pointMultiply( pointSubstract( global_best, pc ), ( c2 * random.random()) )        
    )

    # stop velocity explosion
    vn.trim_to_limits( vel_max, vel_min )

    # update particle's velocity
    particle.set_velocity( vn )    
    # update particle's position by adding the velocity
    particle.set_current( pointSum( pc, vn ) )


def pointSubstract( p1, p2 ):
    return Point( ( p1.get_x() - p2.get_x() ), ( p1.get_y() - p2.get_y() ) )

def pointSum( *args ):    
    x = 0
    y = 0    
    for a in args:
        x += a.get_x()
        y += a.get_y()
    return Point( x, y )
    #return Point( ( p1.get_x() + p2.get_x() ), ( p1.get_y() + p2.get_y() ) )

def pointMultiply( p1, factor ):
    return Point( ( p1.get_x() * factor ), ( p1.get_y() * factor ) )
    #return Point( ( p1.get_x() * p2.get_x() ), ( p1.get_y() * p2.get_y() ) )


def test_function( p ):
    return 3 * pow( p.get_x(), 2 ) - 2 * p.get_x() * p.get_y() + 3 * pow( p.get_y(), 2 ) - p.get_x() - p.get_y()


# minimum is at the following point for the above function
# 6x1 - 2x2 - 1 = 0 & 6x2 - 2x1 - 1 = 0
def test_fitness_function( p ):
    
    f1 = 6 * p.get_x() - 2 * p.get_y() - 1
    f2 = 6 * p.get_y() - 2 * p.get_x() - 1
    f = abs( f1 ) + abs( f2 )
    return 1 / ( 1 + f )

#%%        